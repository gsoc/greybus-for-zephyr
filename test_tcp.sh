#!/bin/bash -ex
echo "Running the POSIX target over TCP transport"
pwd

# https://stackoverflow.com/questions/9689498/feedback-stdin-and-stdout-of-two-processes
function TARGET {
	./build/greybus/net/zephyr/zephyr.exe 
}

function TESTER {
	echo "Hello"
}

[ -e target-input ] || mkfifo target-input
[ -e target-output ] || mkfifo target-output

TARGET <target-input | tee log.txt > target-output &
timeout 50 [ TESTER >target-input <target-output ]

cat log.txt
exit 0
