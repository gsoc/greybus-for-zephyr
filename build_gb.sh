#!/bin/bash -ex

# Build greybus
git clone -b friedt https://github.com/jadonk/greybus.git $CI_PROJECT_DIR/greybus
cd $CI_PROJECT_DIR/greybus
make

# Build gbridge
git clone https://github.com/jadonk/gbridge/ $CI_PROJECT_DIR/gbridge
cd $CI_PROJECT_DIR/gbridge
autoreconf --install
autoconf
automake --add-missing
GBDIR=$CI_PROJECT_DIR/greybus ./configure --enable-netlink --enable-tcpip --enable-uart
make

